package com.devcamp.s50.devcamp_car.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.devcamp_car.model.CCarType;

public interface ICarTypeRepository extends JpaRepository<CCarType, Integer> {
    
}
