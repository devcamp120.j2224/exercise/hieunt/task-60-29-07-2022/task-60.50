package com.devcamp.s50.devcamp_car.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.devcamp_car.model.CCar;

public interface ICarRepository extends JpaRepository<CCar, Integer> {
    CCar findByCarCode(String car);
}
