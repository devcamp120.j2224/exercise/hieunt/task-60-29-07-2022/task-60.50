package com.devcamp.s50.devcamp_car.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.devcamp_car.model.CCar;
import com.devcamp.s50.devcamp_car.model.CCarType;
import com.devcamp.s50.devcamp_car.repository.ICarRepository;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*" , maxAge = -1)
public class CarController {
    @Autowired
    ICarRepository carRepository;
    
    @GetMapping("/devcamp-cars")
    public ResponseEntity<List<CCar>> getAllCars() {
        try {
            List<CCar> listCar = new ArrayList<CCar>();
            carRepository.findAll().forEach(listCar::add);
            return new ResponseEntity<List<CCar>>(listCar, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-cartypes")
    public ResponseEntity<Set<CCarType>> getCarTypesByCarCode(@RequestParam String carCode) {
        try {
            CCar vCar = carRepository.findByCarCode(carCode);
            if (vCar != null) {
                return new ResponseEntity<>(vCar.getCarTypes() , HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
